﻿using Nyanza.Helper;
using Nyanza.Model;
using Nyanza.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nyanza
{
    public partial class Form1 : Form
    {
        CookieContainer container;
        string _filename="";
        List<string> _filenames;
        public Form1()
        {
            InitializeComponent();

            this.Load += (s, a) => {
                //ListviewUtil.AddLVItem("A", "Ziggy", 1,lv_job);
                //ListviewUtil.AddLVItem("B", "Zacky", 1, lv_job);
                //ListviewUtil.AddLVItem("C", "Zoey", 1, lv_job);
                //ListviewUtil.AddLVItem("D", "Zeke", 1, lv_job);
            };
            var file_name = Path.Combine("C://", ConfigurationManager.AppSettings["mappingFile"]);
            //Path.Combine(Application.StartupPath, "amplify_etl_mapping.xlsx");
            var helper = new MappingHelper(file_name);
            helper.load_mappings();

            //load the funding mechanism
            //foreach(var funding in helper.fundings)
            //{
            //    cmbFunding.Items.Add(funding.name);
            //}

            //var states = helper.facilities.Select(e => e.province).Distinct();
            //foreach(var st in states)
            //{
            //    cmbState.Items.Add(st);
            //}

            //cmbState.SelectedIndexChanged += (s, a) => {
            //    var selected = cmbState.SelectedItem.ToString();
            //    var facilities = helper.facilities.Where(e => e.province == selected);
            //    cmbFacility.Items.Clear();
            //    foreach(var facility in facilities)
            //    {

            //        cmbFacility.Items.Add(facility.sdf);
            //    }
            //};

            ////load filenames in the folder into the combobox
            //_filenames = new List<string>();
            //foreach (string filePath in Directory.EnumerateFiles(ConfigurationManager.AppSettings["dataToUpdateFolder"]))
            //{
            //    var file = new FileInfo(filePath);
            //    if (file.Extension == ".xls" || file.Extension == ".xlsx")
            //    {
            //        _filenames.Add(filePath);
            //        cmbFilenames.Items.Add(filePath);
            //    }
            //}

            ////update value of filename when changed
            //cmbFilenames.SelectedIndexChanged += (s, a) =>
            //{
            //    _filename = cmbFilenames.SelectedItem.ToString();
            //};

            button1.Click += (s, a) =>
            {
                var selectedFolderPath = folderBrowserDialog1.SelectedPath;
                _filenames = new List<string>();
                foreach (string filePath in Directory.EnumerateFiles(selectedFolderPath))
                {
                    var file = new FileInfo(filePath);
                    if (file.Extension == ".xls" || file.Extension == ".xlsx")
                    {
                        _filenames.Add(filePath);
                        //cmbFilenames.Items.Add(filePath);
                    }
                }
                var _filenamess = _filenames;
                //if (_filename == null || _filename == "")
                //{
                //    MessageBox.Show("Please select a file");
                //    return;
                //}

                if (container == null)
                {
                    MessageBox.Show("Please login first.", "Access denied");
                    return;
                }

                //if (cboPeriod.SelectedIndex < 0)
                //{
                //    MessageBox.Show("Please select the period", "Config Error");
                //    return;
                //}


                //var facilities = cmbFacility.CheckedItems;
                var job = new ETLJob();
                job.facilities = helper.facilities;
                job.mappings = helper.mappings;
                job.thematicAreas = helper.thematicAreas;
               //job.period = new Period { name = "Quarter 1", code = cboPeriod.SelectedItem.ToString() };
                

                var jhelper = new JobHelper(job, _filenames, container, txtInfoBox);
                var result=jhelper.run_job();
                //"DATIM_Summary_Template.xlsx"
                MessageBox.Show("File upload successful " + result, "Update");
            };

           
            //login();

           
        }

        private void login()
        {
            // dhisLogin loginDetails = (dhisLogin)cmbLogin.SelectedItem;

            //string loginData = "j_username=" + loginDetails.userName + "&" + "j_password=" + loginDetails.password;
            string loginData = "j_username="+txtUsername.Text + "&" + "j_password="+txtPassword.Text;

            string loginPageAddress = ConfigurationManager.AppSettings["apiLoginUrl"];
            var request = (HttpWebRequest)WebRequest.Create(loginPageAddress);
            request.Method = "POST";
            //request.Headers.Add("Accept: application/json, text/javascript, */*; q=0.01");
            request.Headers.Add("authority: cihebdraws.org");
            request.Headers.Add("method: POST");
            request.Headers.Add("path: /dhis-web-commons-security/login.action");
            request.Headers.Add("scheme: https");

            request.Headers.Add("Accept-Encoding: gzip, deflate");
            request.Headers.Add("Accept-Language: en-US,en;q=0.8");
            //request.Headers.Add("Connection: keep-alive");

            request.ContentType = "application/x-www-form-urlencoded";
            //request.//.Add("Host: www.ngusgmerhmis.net");
            //request.Headers.Add("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36");
            request.Headers.Add("X-Requested-With: XMLHttpRequest");

            var buffer = Encoding.ASCII.GetBytes(loginData.ToString());
            request.ContentLength = buffer.Length;


            var requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);
            requestStream.Close();

            container = request.CookieContainer = new CookieContainer();

            var response = request.GetResponse();
            response.Close();
            string test = response.ResponseUri.OriginalString;
            if (test == ConfigurationManager.AppSettings["DashboardUrl"])
                FormUtil.DisplayInfo(DateTime.Now.ToString() + ": login as " + ""+ " was successful", "",txtInfoBox);
            else
                FormUtil.DisplayInfo(DateTime.Now.ToString() + ": login failed", "",txtInfoBox);




        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text == "")
            {
                txtUsername.Focus();
                MessageBox.Show("Enter your username", "Login Error !");
                return;
            }

            if (txtPassword.Text == "")
            {
                txtPassword.Focus();
                MessageBox.Show("Enter your password", "Login Error !");
                return;
            }
            login();
        }

        //private void button4_Click(object sender, EventArgs e)
        //{
        //    if (openFileDialog1.ShowDialog() == DialogResult.OK)
        //    {
        //        if (openFileDialog1.FileName == string.Empty)
        //        {
        //            MessageBox.Show(@"There is no file selected for this operation.");
        //            return;
        //        }
        //        var file = new FileInfo(openFileDialog1.FileName);
        //        if (file.Extension == ".xls" || file.Extension == ".xlsx")
        //        {
        //            _filename = openFileDialog1.FileName;
        //            txtFileName.Text = openFileDialog1.SafeFileName;
        //        }
        //    }
        //}

        //new method
        private void button4_Click(object sender, EventArgs e)
        {
            foreach (string filePath in Directory.EnumerateFiles(ConfigurationManager.AppSettings["dataToUpdateFolder"]))
            {
                var file = new FileInfo(filePath);
                if (file.Extension == ".xls" || file.Extension == ".xlsx")
                {
                    _filenames = new List<string>();
                    _filenames.Add(filePath);
                }
            }

            //add code to add _filenames to the combobox
            //foreach (var filename in _filenames)
            //{
            //    cmbFacility.Items.Add(filename);
            //}
        }

        //private void button2_Click(object sender, EventArgs e)
        //{
        //    for(int i = 0; i < cmbFacility.Items.Count; i++)
        //        cmbFacility.SetItemChecked(i, true);
        //}

        //private void button3_Click(object sender, EventArgs e)
        //{
        //    for (int i = 0; i < cmbFacility.Items.Count; i++)
        //        cmbFacility.SetItemChecked(i, false);

        //}

        private void cmbFacility_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {

        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        //Button to select folder
        private void button4_Click_2(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
        }

        //private void btnSend_Click(object sender, EventArgs e)
        //{
        //    var selectedFolderPath = folderBrowserDialog1.SelectedPath;
        //    _filenames = new List<string>();
        //    foreach (string filePath in Directory.EnumerateFiles(selectedFolderPath))
        //    {
        //        var file = new FileInfo(filePath);
        //        if (file.Extension == ".xls" || file.Extension == ".xlsx")
        //        {
        //            _filenames.Add(filePath);
        //            //cmbFilenames.Items.Add(filePath);
        //        }
        //    }
        //    var _filenamess = _filenames;
        //}

        private void cboPeriod_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
