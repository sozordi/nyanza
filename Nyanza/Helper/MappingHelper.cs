﻿using Nyanza.Model;
using Nyanza.Utils;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nyanza.Helper
{
    public class MappingHelper
    {
        string _file_name;
        public List<ETLMapping> mappings;
        public List<Facility> facilities;
        public List<ThematicAreas> thematicAreas;
        public List<Funding> fundings;

       

        public MappingHelper(string file_name)
        {
            _file_name = file_name;
            mappings = new List<ETLMapping>();
            facilities = new List<Facility>();
            //thematicAreas = new List<ThematicAreas>();
            //fundings = new List<Funding>();
               
        }

        public void load_mappings()
        {
            if (File.Exists(_file_name))
            {
                MessageBox.Show("File found at " + _file_name, "Update");
                using (var package=new ExcelPackage(new FileInfo(_file_name)))
                {
                    //load the sdf
                    var worksheet = package.Workbook.Worksheets["Facility"];
                    for(var i = 2; i < 5000; i++)
                    {
                        if (worksheet.Cells["D" + i] == null || worksheet.Cells["D" + i].Value == null || worksheet.Cells["D" + i].Value.ToString() == "") break;

                        var facility = new Facility();
                        facility.datim_code = ExcelUtil.GetStringValue(worksheet.Cells["D" + i]);
                        facility.province = ExcelUtil.GetStringValue(worksheet.Cells["A" + i]);
                        facility.sdf = ExcelUtil.GetStringValue(worksheet.Cells["E" + i]);
                        facility.moh_ou_code = ExcelUtil.GetStringValue(worksheet.Cells["C" + i]);
                        //facility.country = ExcelUtil.GetStringValue(worksheet.Cells["E" + i]);
                        //facility.province = ExcelUtil.GetStringValue(worksheet.Cells["F" + i]);
                        //facility.county = ExcelUtil.GetStringValue(worksheet.Cells["G" + i]);
                        //facility.orgunitlevel4 = ExcelUtil.GetStringValue(worksheet.Cells["H" + i]);
                        //facility.orgunitlevel5 = ExcelUtil.GetStringValue(worksheet.Cells["I" + i]);
                        //facility.sdf = ExcelUtil.GetStringValue(worksheet.Cells["J" + i]);
                        //facility.project = ExcelUtil.GetStringValue(worksheet.Cells["K" + i]);
                        //facility.services = ExcelUtil.GetStringValue(worksheet.Cells["L" + i]);
                        
                        facilities.Add(facility);
                    }


                    //load services
                    /*
                    worksheet = package.Workbook.Worksheets["services"];

                    for (var i = 2; i < 100; i++)
                    {
                        if (worksheet.Cells["A" + i] == null || worksheet.Cells["A" + i].Value == null || worksheet.Cells["A" + i].Value.ToString() == "") break;

                        var thematic_area = new ThematicAreas();
                        thematic_area.service_name = ExcelUtil.GetStringValue(worksheet.Cells["A" + i]);
                        thematic_area.code = ExcelUtil.GetStringValue(worksheet.Cells["B" + i]);

                        thematicAreas.Add(thematic_area);

                    }*/

                    //load mapping
                    worksheet = package.Workbook.Worksheets["Mapped"];
                    for(var i = 2; i < 10000; i++)
                    {
                        if (worksheet.Cells["A" + i] == null || worksheet.Cells["A" + i].Value == null || worksheet.Cells["A" + i].Value.ToString() == "") break;

                      //  if (worksheet.Cells["J" + i] == null || worksheet.Cells["J" + i].Value ==null || worksheet.Cells["J" + i].Value.ToString()=="") continue;

                        var mapping = new ETLMapping();
                        mapping.shortname = ExcelUtil.GetStringValue(worksheet.Cells["A" + i]);
                        //mapping.thematic_area = ExcelUtil.GetStringValue(worksheet.Cells["B" + i]);
                        //mapping.data_element_desc = ExcelUtil.GetStringValue(worksheet.Cells["F" + i]);
                        mapping.categoryoptioncombo = ExcelUtil.GetStringValue(worksheet.Cells["G" + i]);
                        mapping.dataset = ExcelUtil.GetStringValue(worksheet.Cells["C" + i]);
                        mapping.data_element = ExcelUtil.GetStringValue(worksheet.Cells["A" + i]);
                        mapping.code = ExcelUtil.GetStringValue(worksheet.Cells["D" + i]);
                        mapping.dataelementuid = ExcelUtil.GetStringValue(worksheet.Cells["B" + i]);
                        mapping.categoryoptioncombocode = ExcelUtil.GetStringValue(worksheet.Cells["F" + i]);
                        //mapping.categoryoptioncombouid = ExcelUtil.GetStringValue(worksheet.Cells["I" + i]);
                        mapping.datim_mapping_logic = ExcelUtil.GetStringValue(worksheet.Cells["H" + i]);

                        mappings.Add(mapping);
                    }


                    /*worksheet = package.Workbook.Worksheets["mechanism"];

                    for (var i = 1; i < 15; i++)
                    {
                        if (worksheet.Cells["A" + i] == null || worksheet.Cells["A" + i].Value == null || worksheet.Cells["A" + i].Value.ToString() == "") break;

                        fundings.Add(new Funding { name = ExcelUtil.GetStringValue(worksheet.Cells["A" + i]), code = ExcelUtil.GetStringValue(worksheet.Cells["B" + i]) });

                    }*/
                }
            }
            else
            {
                MessageBox.Show("File not found at " + _file_name, "Update");
            }
        }

    }
}
