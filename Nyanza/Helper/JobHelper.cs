﻿using Nyanza.Model;
using Nyanza.Utils;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nyanza.Helper
{
    public class JobHelper
    {
        CookieContainer container;
        ETLJob _job;
        string _filename;
        List<string> _filenames;
        RichTextBox infoTxt;

        public JobHelper(ETLJob job,List<string> filenames,CookieContainer cntner,RichTextBox richTextBox)
        {
            _filenames = filenames;
            _job = job;
            container = cntner;
            infoTxt = richTextBox;
        }

        public int run_job()
        {
            string apiDataValuesUrl = ConfigurationManager.AppSettings["apiDataValuesUrl"];
            var count = 1;
            FormUtil.DisplayInfo(_job.mappings.Count().ToString() + " Mapping found", "success", infoTxt);
            foreach (var filename in _filenames)
            {

                using (var package = new ExcelPackage(new FileInfo(filename)))
                {
                    var tb_maps = _job.mappings.Where(e => e.thematic_area.StartsWith("TB")).GroupBy(e=>e.thematic_area);
                
                    foreach(var mapping in _job.mappings)
                        {
                    
                        //get the logic
                        //if (mapping.datim_mapping_logic == "TB_PREV_Den:F6")
                        //{

                        //}

                        var logic_config = mapping.datim_mapping_logic.Split(':');
                        string valueAddress = logic_config[1];
                        var worksheet = package.Workbook.Worksheets[logic_config[0]];
                        ////Select all cells in column d between 9990 and 10000
                        //var query1 = (from cell in worksheet.Cells["d:d"] where cell.Value != null select cell);

                        if (worksheet.Cells[valueAddress] != null && worksheet.Cells[valueAddress].Value != null && worksheet.Cells[valueAddress].Value.ToString() != "")
                        {

                            //var org_unit = worksheet.Cells["A" + i].Value.ToString();
                            //if (_job.facilities.Where(e => e.datim_code == org_unit).Any())

                            //old cells
                            //var mflCode = worksheet.Cells["Y2"].Value.ToString();
                            //var date = worksheet.Cells["AN2"].Value.ToString() + worksheet.Cells["AQ2"].Value.ToString();

                            //new cells
                            var mflCode = worksheet.Cells["AE1"].Value.ToString();
                            var date = worksheet.Cells["BE1"].Value.ToString() + worksheet.Cells["AN1"].Value.ToString();
                            ////if date.

                            foreach (Facility f in _job.facilities)
                            {
                                if (mflCode == f.moh_ou_code)
                                {

                                    var org_unit = f.datim_code; 
                                    //var services = _job.facilities.FirstOrDefault(e => e.datim_code == org_unit).services.Split('|');
                                    // if (!services.Contains(logic_config[0])) continue;

                                    var value = ExcelUtil.GetStringValue(worksheet.Cells[logic_config[1].Substring(0, logic_config[1].Length)]);
                           

                                    //if (mapping.datim_mapping_logic == "TB_PREV_Den:F6")
                                    //{

                                    //}


                                    //var dd = logic_config[1].Substring(0, logic_config[1].Length) + i;

                           
                                    if (value != "")
                                    {
                                        if (value.ToLower() == "nd")
                                        {
                                            value = "";
                                        }
                                        var request = (HttpWebRequest)HttpWebRequest.Create(apiDataValuesUrl);
                                        request.Method = "POST";
                                        request.Accept = "application/json, text/javascript, */*; q=0.01";
                                        request.Headers.Add("Accept-Encoding: gzip, deflate");
                                        request.Headers.Add("Accept-Language: en-US,en;q=0.8");
                                        request.KeepAlive = true;

                                        request.ContentType = "application/x-www-form-urlencoded";
                                        //request.Host = "cihebdraws.org";
                                        request.Headers.Add("authority: cihebdraws.org");
                                        request.Headers.Add("scheme: https");
                                        request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36";
                                        request.Headers.Add("X-Requested-With: XMLHttpRequest");

                                        //string postStringValue = "de=" + mapping.dataelementuid + "&co=" + mapping.categoryoptioncombocode + "&ou=" + org_unit + "&pe=" + _job.period.code + "&value=" + value + "&cc=wUpfppgjEza&cp=l3ofCXWl8Jy&ds=tz1bQ3ZwUKJ";
                                        //de=KpZjsiYZQd4&co=MIFuhOzrEJl&ds=clGoP0uwSST&ou=vNuHvXbWbIo&pe=202110&value =

                                        //someting is wrong
                                       //var postStringValue = "de=" + mapping.dataelementuid + "&co=" + mapping.categoryoptioncombocode + "&ds="+mapping.dataset+"&ou=" + org_unit + "&pe=202110&value=" + value;
                                        var postStringValue = "de=" + mapping.dataelementuid + "&co=" + mapping.categoryoptioncombocode + "&ds="+mapping.dataset+"&ou=" + org_unit + "&pe=" + date + "&value=" + value;

                                        //var postStringValue = "de=KpZjsiYZQd4&co=MIFuhOzrEJl&ds=clGoP0uwSST&ou=vNuHvXbWbIo&pe=202110&value="+ value;

                                        //postStringValue = "de=RhkU5KjtAi6&co=eIV9Qheksp8&ds=KWRj80vEfHU&ou=uTCWMoNzn68&pe=2018Q4&value=5&cc=wUpfppgjEza&cp=Su7njIGU1aA";
                                        var postData = Encoding.ASCII.GetBytes(postStringValue);
                                        try
                                        {
                                            request.CookieContainer = container;
                                            request.ContentLength = postData.Length;

                                            var requestStream = request.GetRequestStream();
                                            requestStream.Write(postData, 0, postData.Length);
                                            requestStream.Close();

                                            //FormUtil.DisplayInfo(postStringValue, "success", infoTxt);
                                            var response = request.GetResponse();

                                            container = request.CookieContainer;
                                            response.Close();

                                            FormUtil.DisplayInfo(_job.facilities.Where(e => e.datim_code == org_unit).FirstOrDefault().sdf + " | " + mapping.data_element_desc + " | " + mapping.categoryoptioncombo + " | Value=" + value + " >> success", "success", infoTxt);
                                            count++;
                                        }
                                        catch (Exception ex)
                                        {

                                            FormUtil.DisplayInfo(_job.facilities.Where(e => e.datim_code == org_unit).FirstOrDefault().sdf + " | " + mapping.data_element_desc + " | " + mapping.categoryoptioncombo + " | Value=" + value + " >> failed | Message=" + ex.Message + "|string Value =" + postStringValue, "error", infoTxt);
                                            //DisplayInfo(fac.dhis_org_name + ": Element =" + row.dhis_indcator_code_1 + " | Value=" + value + " >> retrying", "retrying");
                                        }

                                    }

                                }
                            }
                        }

                    



                        //for (var i = 3; i < 5000; i++)
                        //{
                        //    if (worksheet.Cells["A" + i] == null || worksheet.Cells["A" + i].Value == null || worksheet.Cells["A" + i].Value.ToString() == "") continue;

                        //    var org_unit = worksheet.Cells["A" + i].Value.ToString();
                        //    if (_job.facilities.Where(e => e.datim_code == org_unit).Any())
                        //    {
                        //        //var services = _job.facilities.FirstOrDefault(e => e.datim_code == org_unit).services.Split('|');
                        //        // if (!services.Contains(logic_config[0])) continue;

                        //        var value = ExcelUtil.GetStringValue(worksheet.Cells[logic_config[1].Substring(0, logic_config[1].Length) + i]);

                        //        //if (mapping.datim_mapping_logic == "TB_PREV_Den:F6")
                        //        //{

                        //        //}


                        //        var dd = logic_config[1].Substring(0, logic_config[1].Length) + i;
                        //        if (logic_config[0].StartsWith("TB"))
                        //        {

                        //        }

                        //        if (value != "")
                        //        {
                        //            if (value.ToLower() == "nd")
                        //            { 
                        //                value = ""; 
                        //            }
                        //            var request = (HttpWebRequest)HttpWebRequest.Create(apiDataValuesUrl);
                        //            request.Method = "POST";
                        //            request.Accept = "application/json, text/javascript, */*; q=0.01";
                        //            request.Headers.Add("Accept-Encoding: gzip, deflate");
                        //            request.Headers.Add("Accept-Language: en-US,en;q=0.8");
                        //            request.KeepAlive = true;

                        //            request.ContentType = "application/x-www-form-urlencoded";
                        //            request.Host = "www.datim.org";
                        //            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36";
                        //            request.Headers.Add("X-Requested-With: XMLHttpRequest");

                        //            //string postStringValue = "de=" + mapping.dataelementuid + "&co=" + mapping.categoryoptioncombocode + "&ou=" + org_unit + "&pe=" + _job.period.code + "&value=" + value + "&cc=wUpfppgjEza&cp=l3ofCXWl8Jy&ds=tz1bQ3ZwUKJ";
                        //            //de=KpZjsiYZQd4&co=MIFuhOzrEJl&ds=clGoP0uwSST&ou=vNuHvXbWbIo&pe=202110&value =
                        //            var postStringValue = "de=" + mapping.dataelementuid + "&co=" + mapping.categoryoptioncombocode + "&ds=jKdHXpBfWop&ou=" + org_unit + "&pe=" + _job.period.code + "&value=" + value + "&cc=wUpfppgjEza&cp=" + _job.funding.code;

                        //            //postStringValue = "de=RhkU5KjtAi6&co=eIV9Qheksp8&ds=KWRj80vEfHU&ou=uTCWMoNzn68&pe=2018Q4&value=5&cc=wUpfppgjEza&cp=Su7njIGU1aA";
                        //            var postData = Encoding.ASCII.GetBytes(postStringValue);
                        //            try
                        //            {
                        //                request.CookieContainer = container;
                        //                request.ContentLength = postData.Length;

                        //                var requestStream = request.GetRequestStream();
                        //                requestStream.Write(postData, 0, postData.Length);
                        //                requestStream.Close();

                        //                var response = request.GetResponse();

                        //                container = request.CookieContainer;
                        //                response.Close();

                        //                FormUtil.DisplayInfo(_job.facilities.Where(e => e.datim_code == org_unit).FirstOrDefault().sdf + " | " + mapping.data_element_desc + " | " + mapping.categoryoptioncombo + " | Value=" + value + " >> success", "success", infoTxt);
                        //                count++;
                        //            }
                        //            catch (Exception ex)
                        //            {

                        //                FormUtil.DisplayInfo(_job.facilities.Where(e => e.datim_code == org_unit).FirstOrDefault().sdf + " | " + mapping.data_element_desc + " | " + mapping.categoryoptioncombo + " | Value=" + value + " >> failed | Message=" + ex.Message, "error", infoTxt);
                        //                //DisplayInfo(fac.dhis_org_name + ": Element =" + row.dhis_indcator_code_1 + " | Value=" + value + " >> retrying", "retrying");
                        //            }

                        //        }
                        //    }

                        //}

                    }
                
                }

            }

            return count;
        }
    }
}
