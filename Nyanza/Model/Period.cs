﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nyanza.Model
{
    public class Period
    {
        public string code { set; get; }
        public string name { set; get; }
    }
}
