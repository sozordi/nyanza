﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nyanza.Model
{
    public class Facility
    {
        public string country_code { set; get; }
        public string datim_code { set; get; }
        public string pm_ou_code {set;get;}
        public string moh_ou_code { set; get; }
        public string country { set; get; }
        public string province { set; get; }
        public string county { set; get; }
        public string orgunitlevel4 { set; get; }
        public string orgunitlevel5 { set; get; }
        public string sdf { set; get; }
        public string project { set; get; }
        public string services { set; get; }

    }
}
