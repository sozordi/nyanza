﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nyanza.Model
{
    public class ETLJob
    {
        public List<ETLMapping> mappings { set; get; }
        public List<Facility> facilities { set; get; }
        public List<ThematicAreas> thematicAreas { set; get; }
        public Period period { set; get; }
        public Funding funding { set; get; }
    }
}
