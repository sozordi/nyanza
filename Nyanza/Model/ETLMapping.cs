﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nyanza.Model
{
    public class ETLMapping
    {
        public string shortname { set; get; }
        public string thematic_area { set; get; }
        public string data_element_desc { set; get; }
        public string categoryoptioncombo { set; get; }
        public string dataset { set; get; }
        public string data_element { set; get; }
        public string code { set; get; }
        public string dataelementuid { set; get; }
        public string categoryoptioncombocode { set; get; }
        public string categoryoptioncombouid { set; get; }
        public string datim_mapping_logic { set; get; }
    }
}
