﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nyanza.Utils
{
    public static class FormUtil
    {
        public static void DisplayInfo(string msg, string type,RichTextBox txtInfoBox)
        {
            Color color = System.Drawing.Color.Black;
            if (type == "warning") color = System.Drawing.Color.Orange;
            if (type == "error") color = System.Drawing.Color.Red;
            if (type == "success") color = System.Drawing.Color.Green;
            if (type == "retrying") color = System.Drawing.Color.Blue;
            txtInfoBox.SelectionStart = txtInfoBox.TextLength;
            txtInfoBox.SelectionLength = 0;
            txtInfoBox.SelectionColor = color;
            txtInfoBox.AppendText("\r\n" + msg);
            txtInfoBox.ScrollToCaret();
            System.Windows.Forms.Application.DoEvents();
        }
    }
}
