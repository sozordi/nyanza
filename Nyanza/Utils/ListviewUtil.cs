﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nyanza.Utils
{
    public static class ListviewUtil
    {
        public static void AddLVItem(string key, string name, int value,ListView lv)
        {
            ListViewItem lvi = new ListViewItem();
            ProgressBar pb = new ProgressBar();

            lvi.SubItems[0].Text = name;
            lvi.SubItems.Add(value.ToString());
            lvi.SubItems.Add("");
            lvi.SubItems.Add(key);            
            lv.Items.Add(lvi);


            value = new Random().Next(100);

            Rectangle r = lvi.SubItems[2].Bounds;
            pb.SetBounds(r.X, r.Y, r.Width, r.Height);
            pb.Minimum = 1;
            pb.Maximum = 100;
            pb.Value = value;
            pb.Name = key;                   
            lv.Controls.Add(pb);
        }


        public static void UpdateItemValue(string key, int value, ListView lv)
        {
            ListViewItem lvi;
            ProgressBar pb;

           
            lvi = lv.Items.Cast<ListViewItem>().FirstOrDefault(q => q.SubItems[3].Text == key);
            if (lvi != null)
                lvi.SubItems[1].Text = value.ToString();

            pb = lv.Controls.OfType<ProgressBar>().FirstOrDefault(q => q.Name == key);
            if (pb != null)
                pb.Value = value;
        }

    }
}
