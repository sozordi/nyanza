﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nyanza.Utils
{
    public static class ExcelUtil
    {
        public static string GetStringValue(ExcelRange cell)
        {
            if (cell != null && cell.Value != null)
            {
                return cell.Value.ToString();
            }
            return "";
        }
    }
}
